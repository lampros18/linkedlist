#include <stdio.h>
#include <stdlib.h>
#include "LinkedList.h"

//List init
void init(LIST_PTR *head) //Pointer to pointer call by value
{
  *head = NULL;
}
//Pointer to pointer type strutct node

int isEmpty(LIST_PTR head)
{
  return head == NULL;
}

elem data(LIST_PTR p)
{
  return p -> data;
}

unsigned int insert_start(LIST_PTR *head , elem x)
{
  LIST_PTR newnode = (LIST_PTR)malloc(sizeof(LIST_NODE));
  if(newnode == NULL)
  {
    puts("Memory allocation failed");
    return FALSE;
  }
  newnode -> data = x;
  newnode -> next = *head;
  *head = newnode;
  return TRUE;
}

unsigned int insert_after(LIST_PTR p,elem x)
{
    LIST_PTR newnode = (LIST_PTR)malloc(sizeof(LIST_NODE));
    if(newnode == NULL)
    {
      puts("Memory allocation failed!");
      return FALSE;
    }
    newnode -> data = x;
    newnode -> next = p -> next;
    p -> next = newnode;
    return TRUE;
}

unsigned int delete_start(LIST_PTR *head,elem *x)
{
  LIST_PTR current;

  if(*head == NULL)
  {
    puts("Empty List");
    return FALSE;
  }
  current=*head;
  *x = current -> data;
  (*head) = (*head ) -> next;
  free(current);
  return TRUE;
}

unsigned int delete_after(LIST_PTR prev,elem *x)
{
  LIST_PTR current;
  if(prev -> next == NULL)
  {
    puts("Empty List!");
    return FALSE;
  }
  current = prev -> next;
  *x = current -> data;
  prev -> next = current -> next;
  free(current);
  return TRUE;
}

void destroy(LIST_PTR *head)
{
  LIST_PTR ptr;
  if(*head == NULL)
  {
    puts("List is empty");
  }
  while( (*head) != NULL)
  {
    ptr = (*head);
    (*head) = (*head) -> next;
    free(ptr);
  }
}

void print(LIST_PTR head){
  LIST_PTR current;
  current = head;
  while(current != NULL)
  {
    printf("| [NAME: %s] [SURNAME: %s] [AMOUNT: %lf] |->  ",current -> data.name,current -> data.surname , current -> data.amount );
    current = current -> next;
  }
  printf("%s\n","|NULL|" );
}
