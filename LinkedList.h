#ifndef _LINKED_LIST_H
#define _LINKED_LIST_H

#define TRUE 1
#define FALSE 0

struct client
{
  char name[30];
  char surname[30];
  double amount;
};
typedef struct client CLIENT;



typedef CLIENT elem; //Data type of the list

struct node //Node
{
  elem data; //DATA
  struct node *next; //Pointer to the next node
};
typedef struct node LIST_NODE;
typedef struct node * LIST_PTR;

void init(LIST_PTR *head);
int isEmpty(LIST_PTR head);
unsigned int insert_start(LIST_PTR *head , elem x);
unsigned int insert_after(LIST_PTR p,elem x);
unsigned int delete_start(LIST_PTR *head,elem *x);
unsigned int delete_after(LIST_PTR prev,elem *x);
void destroy(LIST_PTR *head);
void print(LIST_PTR head);


#endif
