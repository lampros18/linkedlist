//
// Created by Lampros on 10/2/2017.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "strio.h"

int clean_stdin(void)//Function that cleans the standard input and returns 1 for success
{
    while (getchar()!='\n');
    return 1;
}



void save_string( char *string , size_t size){
    FILE *fp;
    if (NULL == (fp = fopen("sav.dat" , "wb") )){
        puts("File could not be opened!");
        exit(EXIT_FAILURE);
    }else{
        fwrite( string , sizeof(char) , size + 1 ,fp);
        puts("Data successfully saved to file:: sav.dat\n");
    }
    fclose(fp);
}

void save_string_2d(char string[MAXROWS][MAXSIZE]){
    int i = 0;
    FILE *fp;
    if( (fp = fopen("sav2d.dat","wb")) == NULL ){
        puts("File could not be opened1");
        exit(EXIT_FAILURE);
    }else {

        while (i < MAXROWS) {

            int j = 0;
            while( j < MAXSIZE && string[i][j] != '\0'){
                j++;
            }
            j++;

            fwrite(string[i], sizeof(char), j , fp);
            i++;
        }
    }
    puts("Data successfully written to file sav2d.dat");
    fclose(fp);
}

size_t input ( char *string , size_t size , char * string1) {
    printf("%s\n", string1);
    int m = 0, mem_flag = 0;
    char w;
    while ((w = getchar()) != '\n' && m < size) {
        string[m++] = w;
        if (m == size - 1) {
            string[m] = '\0';
            mem_flag = 1;
            clean_stdin();
            break;
        }
    }

    if (m != size - 1) {
        string[m] = '\0';
    }


    if(mem_flag)
        puts("Attention,you entered 1023 characters,no more allowed");

    return (size_t)m;

}


void input_2d( char string[MAXROWS][MAXSIZE] , size_t size , size_t size1 ,char *string1){
    char w;
    int m = 0 , i = 0;
    int mem_flag = 0;

    while( i < size ) {
        printf("%s\n",string1);
        m=0;
        while ((w = getchar()) != '\n' && m < size1)//Getting input from user
        {
            string[i][m++] = w;


            if (m == size1 - 1) {
                string[i][m] = '\0';
                mem_flag = 1;
                clean_stdin();
                break;
            }
        }
        if (m != size1 - 1) {
            string[i][m] = '\0';
        }
        if (mem_flag)
            puts("Attention,you entered 1023 characters,no more allowed");
        i++;
    }
}

void print_2d(char string[MAXROWS][MAXSIZE]){
    for(int i = 0; i < MAXROWS;i++ ){
        printf("%s\n",string[i]);
    }
}



short load_string( char *string ,size_t size){
    FILE *fp;
    short flag = 0;
    if ( (fp = fopen("sav.dat","rb")) == NULL){
        puts("File could not be opened!");
        return flag;
    }else{
            fread( string , 1 , size  , fp);
            flag = 1;
        }
    fclose(fp);
    return flag;
}


short load_2d ( char string[MAXROWS][MAXSIZE] ){
    FILE *fp;
    short flag = 0;
    if ( (fp = fopen("sav2d.dat","rb")) == NULL){
        puts("File could not be opened!");
        return flag;
    }else {
        int j = 0,i=0,tmp =0;

            char temp[MAXSIZE] = {0};

            fgets(temp, MAXSIZE, fp);
            if(temp == NULL) {
                fprintf(stderr, "Text file corrupted\n");
            }
            size_t counter = ftell(fp);
            rewind(fp);
            while( j < counter  ){

//                printf("%u=j\n",j);

                if(temp[j] == '\0'){
//                    printf("%u=j mesa sto o\n",j);
                    fread(string[i++], 1, j - tmp + 1 , fp);
                    tmp = j+1;//Poustia kai memory hole edw!!!!!!!!!!!!!!!!!!!!1
//                    printf("%u=tmp mesa sto o\n",tmp);
                }
                j++;
            }
        flag = 1;
    }
    fclose(fp);
    return flag;
}
