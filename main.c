#include <stdio.h>
#include <stdlib.h>
#include "LinkedList.h"
#include "strio.h"

void ClientRegister(LIST_PTR *head);

int main(int argc, char const *argv[]) {
  LIST_PTR head;
  unsigned int run = 1;
  int choice;
  init(&head);
  system("clear");
  while(run == 1)
  {
    printf("\n0-Clear the input\n1-Insert Client\n2-Print the clients\n3-Exit\n");
    scanf(" %d",&choice);
    switch (choice) {
      case 0:
      system("clear");
      break;
      case 1:
      ClientRegister(&head);
      break;

      case 2:
      print(head);
      break;

      case 3:
      run = 0;
      break;
    }
  }

  destroy(&head);
  return 0;
}

void ClientRegister(LIST_PTR *head)
{
  CLIENT client;
  clean_stdin();
  input(client.name,30,"Name: ");
  input(client.surname,30,"Surname: ");
  printf("%s","Amount: " );
  scanf("%lf",&client.amount);
  insert_start(head,client);
}
